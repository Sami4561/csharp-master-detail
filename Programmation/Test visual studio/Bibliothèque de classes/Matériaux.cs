﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class Matériaux : Item
    {
        public Matériaux(string nom, string IDNom, string versionAjout, bool empilable, string image, bool renouvelableC)
            : base(nom, IDNom, versionAjout, empilable, image)
        {
            renouvelable = renouvelableC;
        }
        
        public bool Renouvelable
        {
            get => renouvelable;
        }
        [DataMember]
        private readonly bool renouvelable;

        public override string ToString()
        {
            return $"{Nom}{IDNom}{VersionAjout}{Empilable}{Renouvelable}";
        }
    }
}
