﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class Blocs : Item
    {
        public Blocs(string nom, string IDNom, string versionAjout, bool empilable, string image, string outilC)
                : base(nom, IDNom, versionAjout, empilable, image)
        {
            outil = outilC;
        }

        public string Outil
        {
            get => outil;
        }
        [DataMember]
        private readonly string outil;

        public override string ToString()
        {
            return $"{Nom}{IDNom}{VersionAjout}{Empilable}{Outil}";
        }
    }
}
