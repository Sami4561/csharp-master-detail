﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliothèque_de_classes
{
    public interface IPersistanceManager
    {
        (IEnumerable<Item> favoris, IEnumerable<ListeObjets> listes) ChargeDonnées();

        void SauvegardeDonnées(IEnumerable<Item> favoris, IEnumerable<ListeObjets> listes);
    }
}
