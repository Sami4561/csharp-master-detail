﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class ListeObjets : INotifyPropertyChanged
    {
        public ListeObjets(string nom, ObservableCollection<Item> listeItemsC, DateTime dateAjout)
        {
            Nom = nom;
            DateAjout = dateAjout;
            ListeItems = listeItemsC;
        }

        [DataMember]
        public string Nom { get; private set; }

        [DataMember]
        public DateTime DateAjout { get; private set; }

        [DataMember]
        public ObservableCollection<Item> ListeItems { get; set; }
        
        private ObservableCollection<Item> listeItems = new ObservableCollection<Item>();

        public event PropertyChangedEventHandler PropertyChanged;


        public override string ToString()
        {
            return $"{Nom}";
        }

        void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));


        //public Item GetItem(Item item)
        //{
        //    return ListeItems.SingleOrDefault(i => i.Equals(item));
        //}
    }
}
