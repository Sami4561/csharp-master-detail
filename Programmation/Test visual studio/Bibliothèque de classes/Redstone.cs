﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class Redstone : Item
    {
        public Redstone(string nom, string IDNom, string versionAjout, bool empilable, string image)
             : base(nom, IDNom, versionAjout, empilable, image)
        {

        }
        public override string ToString()
        {
            return $"{Nom}{IDNom}{VersionAjout}{Empilable}";
        }
    }
}
