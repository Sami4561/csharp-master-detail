﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class ArmesOutils : Item
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="IDNom"></param>
        /// <param name="versionAjout"></param>
        /// <param name="empilable"></param>
        /// <param name="image"></param>
        /// <param name="durabilitéC">Durabilité de l'objet</param>
        public ArmesOutils(string nom, string IDNom, string versionAjout, bool empilable, string image, int durabilitéC)
             : base(nom, IDNom, versionAjout, empilable, image)
        {
            durabilité = durabilitéC;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="IDNom"></param>
        /// <param name="versionAjout"></param>
        /// <param name="empilable"></param>
        /// <param name="image"></param>
        /// <param name="durabilitéC"></param>
        /// <param name="dégâtsC">Dégâts de l'objet</param>
        public ArmesOutils(string nom, string IDNom, string versionAjout, bool empilable, string image, int durabilitéC, string dégâtsC)
            : base(nom, IDNom, versionAjout, empilable, image)
        {
            durabilité = durabilitéC;
            dégâts = dégâtsC;
        }

        public int Durabilité
        {
            get => durabilité;
        }
        [DataMember]
        private readonly int durabilité;

        public string Dégâts
        {
            get => dégâts;
        }
        [DataMember]
        private readonly string dégâts;

        public override string ToString()
        {
            return $"{Nom} {IDNom} {VersionAjout} {Empilable} {Durabilité} {Dégâts}";
        }
    }
}

