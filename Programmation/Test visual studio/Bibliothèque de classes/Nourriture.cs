﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class Nourriture : Item
    {
        public Nourriture(string nom, string IDNom, string versionAjout, bool empilable, string image, int restaurationC, double saturationC)
                : base(nom, IDNom, versionAjout, empilable, image)
        {
            restauration = restaurationC;
            saturation = saturationC;
        }

        public int Restauration
        {
            get => restauration;
        }
        [DataMember]
        private readonly int restauration;

        public double Saturation
        {
            get => saturation;
        }
        [DataMember]
        private readonly double saturation;

        public override string ToString()
        {
            return $"{Nom}{IDNom}{VersionAjout}{Empilable}{Restauration}{Saturation}";
        }
    }
}
