﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class Manager : INotifyPropertyChanged
    {
        /// <summary>
        /// dépendance vers le gestionnaire de la persistance
        /// </summary>
        public IPersistanceManager Persistance { get; /*private*/ set; }

        /// <summary>
        /// Liste des listes d'objets
        /// </summary>
        public ReadOnlyObservableCollection<ListeObjets> Listes { get; private set; }
        [DataMember]
        ObservableCollection<ListeObjets> listes = new ObservableCollection<ListeObjets>();

        /// <summary>
        /// Liste des objets
        /// </summary>
        public ReadOnlyCollection<Item> Items { get; set; }
        List<Item> items = new List<Item>();

        /// <summary>
        /// Liste des favoris
        /// </summary>
        public ReadOnlyObservableCollection<Item> Favoris { get; set; }
        [DataMember]
        ObservableCollection<Item> favoris = new ObservableCollection<Item>();

        public IEnumerable<Item> ItemsArmesOutils { get; set; }
        public IEnumerable<Item> ItemsBlocs { get; set; }
        public IEnumerable<Item> ItemsDécoration { get; set; }
        public IEnumerable<Item> ItemsMatériaux { get; set; }
        public IEnumerable<Item> ItemsNourriture { get; set; }
        public IEnumerable<Item> ItemsPotions { get; set; }
        public IEnumerable<Item> ItemsRedstone { get; set; }
        public IEnumerable<Item> ItemsSpécial { get; set; }
        public IEnumerable<Item> ItemsTransport { get; set; }

        public IEnumerable<Item> FavorisArmesOutils { get; set; }
        public IEnumerable<Item> FavorisBlocs { get; set; }
        public IEnumerable<Item> FavorisDécoration { get; set; }
        public IEnumerable<Item> FavorisMatériaux { get; set; }
        public IEnumerable<Item> FavorisNourriture { get; set; }
        public IEnumerable<Item> FavorisPotions { get; set; }
        public IEnumerable<Item> FavorisRedstone { get; set; }
        public IEnumerable<Item> FavorisSpécial { get; set; }
        public IEnumerable<Item> FavorisTransport { get; set; }

        /// <summary>
        /// Implémente ItemSélectionné lorque l'utilisateur clique sur un objet
        /// </summary>
        private Item itemSélectionné { get; set; }
        public Item ItemSélectionné
        {
            get => itemSélectionné;
            set
            {
                if (itemSélectionné != value)
                {
                    itemSélectionné = value;
                    OnPropertyChanged(nameof(ItemSélectionné));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// Ajoute un item passé en paramètre aux favoris uniquement si Favoris ne le contenait pas
        /// </summary>
        /// <param name="item">ItemSélectionné</param>
        /// <returns></returns>
        public bool AjouteFavori(Item item)
        {
            if (favoris.Contains(item))
            {
                return false;
            }
            favoris.Add(item);
            return true;
        }

        /// <summary>
        /// Supprime un item passé en paramètre uniquement si Favoris le contenait
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool SupprimeFavori(Item item)
        {
            if (favoris.Contains(item) == false)
            {
                return false;
            }
            favoris.Remove(item);
            return true;
        }

        [OnDeserialized]
        void InitReadOnlyObservableCollection(StreamingContext sc = new StreamingContext())
        {
            Listes = new ReadOnlyObservableCollection<ListeObjets>(listes);
            Favoris = new ReadOnlyObservableCollection<Item>(favoris);
        }
        public Manager(IPersistanceManager persistance)
        {
            Persistance = persistance;
            InitReadOnlyObservableCollection();
            Items = new ReadOnlyCollection<Item>(items);
            ItemsArmesOutils = Items.OfType<ArmesOutils>();
            ItemsBlocs = Items.OfType<Blocs>();
            ItemsDécoration = Items.OfType<Décoration>();
            ItemsMatériaux = Items.OfType<Matériaux>();
            ItemsNourriture = Items.OfType<Nourriture>();
            ItemsPotions = Items.OfType<Potions>();
            ItemsRedstone = Items.OfType<Redstone>();
            ItemsSpécial = Items.OfType<Spécial>();
            ItemsTransport = Items.OfType<Transport>();

            FavorisArmesOutils = Favoris.OfType<ArmesOutils>();
            FavorisBlocs = Favoris.OfType<Blocs>();
            FavorisDécoration = Favoris.OfType<Décoration>();
            FavorisMatériaux = Favoris.OfType<Matériaux>();
            FavorisNourriture = Favoris.OfType<Nourriture>();
            FavorisPotions = Favoris.OfType<Potions>();
            FavorisRedstone = Favoris.OfType<Redstone>();
            FavorisSpécial = Favoris.OfType<Spécial>();
            FavorisTransport = Favoris.OfType<Transport>();
        }

        /// <summary>
        /// Charge les données de l'application
        /// </summary>
        public void ChargeDonnées()
        {
            var données = Persistance.ChargeDonnées();
            foreach(var f in données.favoris)
            {
                favoris.Add(f);
            }
            foreach(var l in données.listes)
            {
                listes.Add(l);
            }
            ChargeObjets();
        }

        /// <summary>
        /// Sauvegarde les données de l'application
        /// </summary>
        public void SauvegardeDonnées()
        {
            Persistance.SauvegardeDonnées(favoris, listes); // dépendance
        }

        /// <summary>
        /// Charge les objets dans la liste Items
        /// </summary>
        private void ChargeObjets()
        {
            items.Add(new Décoration("Algue", "kelp", "1.15", true, "img/Algue.png", "test"));
            items.Add(new Matériaux("Algue séchée", "dried_kelp", "1.13", true, "img/Algue_séchée.png", true));
            items.Add(new ArmesOutils("Arbalète", "crossbow", "1.14", false, "img/Arbalète.png", 326, "6-10"));
            items.Add(new ArmesOutils("Arc", "bow", "1.13", false, "img/Arc.png", 326, "6-10"));
            items.Add(new ArmesOutils("Bottes en cuir", "leather_boots", "1.0.0", false, "img/bottes_cuir.png", 66));
            items.Add(new ArmesOutils("Bottes en cotte de mailles", "chainmail_boots", "1.0.0", false, "img/bottes_mailles.png", 196));
            items.Add(new ArmesOutils("Bottes en fer", "iron_boots", "1.0.0", false, "img/bottes_fer.png", 196));
            items.Add(new ArmesOutils("Bottes en or", "golden_boots", "1.0.0", false, "img/bottes_or.png", 92));
            items.Add(new ArmesOutils("Bottes en diamant", "diamond_boots", "1.0.0", false, "img/bottes_diamant.png", 430));
            items.Add(new ArmesOutils("Casque en cuir", "leather_helmet", "1.0.0", false, "img/casque_cuir.png", 56));
            items.Add(new ArmesOutils("Casque en cotte de mailles", "chainmail_helmet", "1.0.0", false, "img/casque_maille.png", 166));
            items.Add(new ArmesOutils("Casque en fer", "iron_helmet", "1.0.0", false, "img/casque_fer.png", 166));
            items.Add(new ArmesOutils("Casque en or", "golden_helmet", "1.0.0", false, "img/casque_or.png", 78));
            items.Add(new ArmesOutils("Casque en diamant", "diamond_helmet", "1.0.0", false, "img/casque_diamant.png", 364));
            items.Add(new ArmesOutils("Plastron en cuir", "leather_chestplate", "1.0.0", false, "img/plastron_cuir.png", 82));
            items.Add(new ArmesOutils("Plastron en cotte de mailles", "chainmail_chestplate", "1.0.0", false, "img/plastron_maille.png", 241));
            items.Add(new ArmesOutils("Plastron en fer", "iron_chestplate", "1.0.0", false, "img/plastron_fer.png", 241));
            items.Add(new ArmesOutils("Plastron en or", "golden_chestplate", "1.0.0", false, "img/plastron_or.png", 114));
            items.Add(new ArmesOutils("Plastron en diamant", "diamond_chestplate", "1.0.0", false, "img/plastron_diamant.png", 529));
            items.Add(new ArmesOutils("Jambières en fer", "iron_leggings", "1.0.0", false, "img/jambières_fer.png", 226));
            items.Add(new ArmesOutils("Jambières en or", "golden_leggings", "1.0.0", false, "img/jambières_or.png", 106));
            items.Add(new ArmesOutils("Jambières en diamant", "diamond_leggings", "1.0.0", false, "img/jambières_diamant.png", 496));
            items.Add(new Blocs("Pierres", "cobblestone", "Pre 0.0.9a", true, "img/Pierres.png","Pioche"));
            items.Add(new Potions("Potion de résistance au feu", "potion", "1.0.0", false, "img/PotionFeu.png", "Invincibilité lave et feu", "Crème de magma", true, false));
            items.Add(new Redstone("Répéteur de redstone", "repeater", "1.0.0", true, "img/RépéteurRedstone.png"));
            items.Add(new Nourriture("Poulet rôti", "cooked_chicken", "1.0.0", true, "img/Poulet_rôti.png", 6, 7.2));
            items.Add(new Transport("Wagonnet", "minecart", "infdev", false, "img/Wagonnet.png", 6));
            items.Add(new Spécial("Sceau de lave", "lava_bucket", "infdev", false, "img/Sceau_de_lave.png"));
        }

        /// <summary>
        /// Implémente ListeSélectionnée lorque l'utilisateur clique sur une liste
        /// </summary>
        private ListeObjets listeSélectionnée { get; set; }

        public ListeObjets ListeSélectionnée
        {
            get => listeSélectionnée;
            set
            {
                if (listeSélectionnée != value)
                {
                    listeSélectionnée = value;
                    OnPropertyChanged(nameof(ListeSélectionnée));
                }
            }
        }

        public ListeObjets GetListeObjets(string nom)
        {
            return listes.SingleOrDefault(l => l.Nom == nom);
        }

        /// <summary>
        /// Ajoute une listeObjets passée en paramètre dans la liste "Listes"
        /// </summary>
        /// <param name="listeObjets">ListeSélectionnée</param>
        /// <returns></returns>
        public bool AjouteListeObjets(ListeObjets listeObjets)
        {
            if (listes.Contains(listeObjets))
            {
                return false;
            }
            listes.Add(listeObjets);
            return true;
        }

        /// <summary>
        /// Supprime une listeObjets passée en paramètre dans la liste "Listes"
        /// </summary>
        /// <param name="listeObjets">ListeSélectionnée</param>
        /// <returns></returns>
        public bool SupprimeListeObjets(ListeObjets listeObjets)
        {
            if (listes.Contains(listeObjets) == false)
            {
                return false;
            }
            listes.Remove(listeObjets);
            return true;
        }

        /// <summary>
        /// Ajoute un objet dans la listeObjetsSélectionné uniquement s'il n'y était pas
        /// </summary>
        /// <param name="item">ItemSélectionné</param>
        /// <returns></returns>
        public bool AjouteItemListe(Item item)
        {
            if (ListeSélectionnée.ListeItems.Contains(item))
            {
                return false;
            }
            ListeSélectionnée.ListeItems.Add(item);
            return true;
        }

        /// <summary>
        /// Supprime un objet dans la listeObjetsSélectionné uniquement s'il y était
        /// </summary>
        /// <param name="item">ItemSélectionné</param>
        /// <returns></returns>
        public bool SupprimeItemListe(Item item)
        {
            if (ListeSélectionnée.ListeItems.Contains(item) == false)
            {
                return false;
            }
            ListeSélectionnée.ListeItems.Remove(item);
            return true;
        }
    }
}

