﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class Transport : Item
    {
        public Transport(string nom, string IDNom, string versionAjout, bool empilable, string image, int pointsDeVieC)
            : base(nom, IDNom, versionAjout, empilable, image)
        {
           pointsDeVie = pointsDeVieC;
        }

        public int PointsDeVie
        {
            get => pointsDeVie;
        }
        [DataMember]
        private readonly int pointsDeVie;

        public override string ToString()
        {
            return $"{Nom}{IDNom}{VersionAjout}{Empilable}{PointsDeVie}";
        }
    }
}
