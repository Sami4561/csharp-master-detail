﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bibliothèque_de_classes
{
    [DataContract]
    public class Potions : Item
    {
        public Potions(string nom, string IDNom, string versionAjout, bool empilable, string image, string effetC, string ingrédientC, bool redstoneC, bool glowstoneC)
            : base(nom, IDNom, versionAjout, empilable, image)
        {
            effet = effetC;
            ingredient = ingrédientC;
            redstone = redstoneC;
            glowstone = glowstoneC;
        }

        public string Effet
        {
            get => effet;
        }
        [DataMember]
        private readonly string effet;
        public bool Redstone
        {
            get => redstone;
        }
        [DataMember]
        private readonly bool redstone;
        public bool Glowstone
        {
            get => glowstone;
        }
        [DataMember]
        private readonly bool glowstone;
        public string Ingredient
        {
            get => ingredient;
        }
        [DataMember]
        private readonly string ingredient;

        public override string ToString()
        {
            return $"{Nom}{IDNom}{VersionAjout}{Empilable}{Effet}{Redstone}{Glowstone}{Ingredient}";
        }
    }
}
