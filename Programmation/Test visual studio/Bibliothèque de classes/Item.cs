﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;

namespace Bibliothèque_de_classes
{
    [DataContract, KnownType(typeof(ArmesOutils)), KnownType(typeof(Blocs)), KnownType(typeof(Décoration)), KnownType(typeof(Matériaux)), KnownType(typeof(Nourriture)), KnownType(typeof(Potions)), KnownType(typeof(Redstone)), KnownType(typeof(Spécial)), KnownType(typeof(Transport))]
    public class Item : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        // C = constructeur
        public Item(string nomC, string idNomC, string versionAjoutC, bool empilableC, string imageC = null)
        {
            nom = nomC;
            idNom = idNomC;
            versionAjout = versionAjoutC;
            empilable = empilableC;
            image = imageC;
        }

        public string Nom
        {
            get => nom;
        }
        [DataMember]
        private readonly string nom;

        public string IDNom
        {
            get => idNom;
        }
        [DataMember]
        private readonly string idNom;

 
        public string VersionAjout
        {
            get => versionAjout;
        }
        [DataMember]
        private readonly string versionAjout;

        
        public bool Empilable
        {
            get => empilable;
        }
        [DataMember]
        private readonly bool empilable;

        public string Image
        {
            get => image;
        }
        [DataMember]
        private readonly string image;

        public override string ToString()
        {
            return $"{Nom}{IDNom}{VersionAjout}{Empilable}{Image}";
        }

        public bool Equals(Item other)
        {
            return Nom.Equals(other.Nom);
        }

        public override bool Equals(object obj)
        {
            //on vérifie si l'objet est null
            if (ReferenceEquals(obj, null)) return false;

            //on vérifie si les deux objets sont égaux
            if (ReferenceEquals(obj, this)) return true;

            if (obj.GetType() != GetType()) return false;
            return Equals(obj as Item);
        }
    }
}
