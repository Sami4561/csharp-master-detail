﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bibliothèque_de_classes;

namespace Test_visual_studio.Windows
{
    /// <summary>
    /// Logique d'interaction pour AjouterObjetListe.xaml
    /// </summary>
    public partial class AjouterObjetListe : Window
    {
        public Manager Manager => (App.Current as App).LeManager;

        public AjouterObjetListe()
        {
            InitializeComponent();
            DataContext = Manager;
        }

        private void Fermer_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
