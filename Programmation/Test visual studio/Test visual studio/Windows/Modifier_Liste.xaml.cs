﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bibliothèque_de_classes;
using Test_visual_studio.Windows;

namespace Test_visual_studio.Windows
{
    /// <summary>
    /// Logique d'interaction pour Modifier_Liste.xaml
    /// </summary>
    public partial class Modifier_Liste : Window
    {
        public Manager Manager => (App.Current as App).LeManager;

        public Modifier_Liste()
        {
            InitializeComponent();
            DataContext = Manager;
        }

        private void Fermer_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
