﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bibliothèque_de_classes;

namespace Test_visual_studio.Windows
{
    /// <summary>
    /// Logique d'interaction pour Créer_Liste.xaml
    /// </summary>
    public partial class Créer_Liste : Window
    {
        public Manager Manager => (App.Current as App).LeManager;

        public ListeObjets LaListeObjets { get; set; }

        public string Nom { get; set; }

        public Créer_Liste()
        {
            InitializeComponent();
            LaListeObjets = new ListeObjets(Nom, new ObservableCollection<Item>(), DateTime.Now);
            DataContext = this;
        }

        private void Annuler_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Créer_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Nom))
            {
                MessageBox.Show("Attention ! Ce nom n'est pas valide !",
                                "Veuillez choisir un autre nom",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Manager.GetListeObjets(Nom) != null)
            {
                MessageBox.Show("Attention ! Ce nom est déjà utilisé pour une autre liste d'objets !",
                                "Veuillez choisir un autre nom",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Manager.AjouteListeObjets(new ListeObjets(Nom, LaListeObjets.ListeItems, LaListeObjets.DateAjout)))
            {
                Close();
                MessageBox.Show("La liste a bien été créée",
                                "Création réussie",
                                MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
    }
}
