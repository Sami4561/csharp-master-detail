﻿using System;
using System.Windows;
using Bibliothèque_de_classes;
using System.Linq;
using System.ComponentModel;
using Test_visual_studio.Windows;
using Test_visual_studio.WindowParts;

namespace Test_visual_studio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Manager Manager => (App.Current as App).LeManager;

        public void App_Closing(Object sender, CancelEventArgs e)
        {
            Manager.SauvegardeDonnées();
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = Manager;

            UC_FavoriDetail.Visibility = Visibility.Visible;
            UC_ItemsMaster.Visibility = Visibility.Visible;
            UC_TousLesFavoris.Visibility = Visibility.Collapsed;
            UC_ListeDetailMainWindow.Visibility = Visibility.Visible;
            UC_ToutesLesListes.Visibility = Visibility.Collapsed;
            UC_ListeDetail.Visibility = Visibility.Collapsed;
        }

        private void Button_TousLesFavoris(object sender, RoutedEventArgs e)
        {
            UC_ItemsMaster.Visibility = Visibility.Collapsed;
            UC_TousLesFavoris.Visibility = Visibility.Visible;
            UC_ToutesLesListes.Visibility = Visibility.Collapsed;
            UC_ListeDetailMainWindow.Visibility = Visibility.Visible;
            UC_ListeDetail.Visibility = Visibility.Collapsed;
            UC_FavoriDetail.Visibility = Visibility.Visible;
        }

        private void Button_PagePrincipale(object sender, RoutedEventArgs e)
        {
            UC_FavoriDetail.Visibility = Visibility.Visible;
            UC_ItemsMaster.Visibility = Visibility.Visible;
            UC_TousLesFavoris.Visibility = Visibility.Collapsed;
            UC_ToutesLesListes.Visibility = Visibility.Collapsed;
            UC_ListeDetailMainWindow.Visibility = Visibility.Visible;
            UC_ListeDetail.Visibility = Visibility.Collapsed;
        }

        private void CréerListe_Click(object sender, RoutedEventArgs e)
        {
            var Créer_Liste = new Créer_Liste();
            Créer_Liste.ShowDialog();
        }

        private void Button_ToutesLesListes(object sender, RoutedEventArgs e)
        {
            UC_ItemsMaster.Visibility = Visibility.Collapsed;
            UC_TousLesFavoris.Visibility = Visibility.Collapsed;
            UC_ToutesLesListes.Visibility = Visibility.Visible;
            UC_FavoriDetail.Visibility = Visibility.Collapsed;
            UC_ListeDetail.Visibility = Visibility.Visible;
            UC_ListeDetailMainWindow.Visibility = Visibility.Collapsed;
        }
    }
}
