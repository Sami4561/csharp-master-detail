﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bibliothèque_de_classes;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Test_visual_studio.WindowParts
{
    /// <summary>
    /// Logique d'interaction pour UC_TousLesFavoris.xaml
    /// </summary>
    public partial class UC_TousLesFavoris : UserControl, INotifyPropertyChanged
    {
        public Manager Manager => (App.Current as App).LeManager;

        public IEnumerable<Item> FavorisArmesOutils { get; set; }
        public IEnumerable<Item> FavorisBlocs { get; set; }
        public IEnumerable<Item> FavorisDécoration { get; set; }
        public IEnumerable<Item> FavorisMatériaux { get; set; }
        public IEnumerable<Item> FavorisNourriture { get; set; }
        public IEnumerable<Item> FavorisPotions { get; set; }
        public IEnumerable<Item> FavorisRedstone { get; set; }
        public IEnumerable<Item> FavorisSpécial { get; set; }
        public IEnumerable<Item> FavorisTransport { get; set; }

        public UC_TousLesFavoris()
        {
            IEnumerable<Item> FavorisArmesOutils = Manager.Favoris.OfType<ArmesOutils>();
            IEnumerable<Item> FavorisBlocs = Manager.Favoris.OfType<Blocs>();
            IEnumerable<Item> FavorisDécoration = Manager.Favoris.OfType<Décoration>();
            IEnumerable<Item> FavorisMatériaux = Manager.Favoris.OfType<Matériaux>();
            IEnumerable<Item> FavorisNourriture = Manager.Favoris.OfType<Nourriture>();
            IEnumerable<Item> FavorisPotions = Manager.Favoris.OfType<Potions>();
            IEnumerable<Item> FavorisRedstone = Manager.Favoris.OfType<Redstone>();
            IEnumerable<Item> FavorisSpécial = Manager.Favoris.OfType<Spécial>();
            IEnumerable<Item> FavorisTransport = Manager.Favoris.OfType<Transport>();

            InitializeComponent();
            DataContext = Manager;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        private void ButtonTriTous(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaTous":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionTous":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxTous.Items.SortDescriptions.Clear();
            ListBoxTous.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriArmesOutils(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaArmesOutils":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionArmesOutils":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxArmesOutils.Items.SortDescriptions.Clear();
            ListBoxArmesOutils.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriBlocs(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaBlocs":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionBlocs":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxBlocs.Items.SortDescriptions.Clear();
            ListBoxBlocs.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriDécoration(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaDécoration":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionDécoration":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxDécoration.Items.SortDescriptions.Clear();
            ListBoxDécoration.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriMatériaux(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaMatériaux":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionMatériaux":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxMatériaux.Items.SortDescriptions.Clear();
            ListBoxMatériaux.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriNourriture(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaNourriture":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionNourriture":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxNourriture.Items.SortDescriptions.Clear();
            ListBoxNourriture.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriPotions(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaPotions":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionPotions":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxPotions.Items.SortDescriptions.Clear();
            ListBoxPotions.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriRedstone(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaRedstone":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionRedstone":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxRedstone.Items.SortDescriptions.Clear();
            ListBoxRedstone.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriSpécial(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaSpécial":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionSpécial":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxSpécial.Items.SortDescriptions.Clear();
            ListBoxSpécial.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }

        private void ButtonTriTransport(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlphaTransport":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriVersionTransport":
                    propertyName = "VersionAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxTransport.Items.SortDescriptions.Clear();
            ListBoxTransport.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }
    }
}
