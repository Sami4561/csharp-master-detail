﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bibliothèque_de_classes;
using Test_visual_studio.Windows;

namespace Test_visual_studio.WindowParts
{
    /// <summary>
    /// Logique d'interaction pour UC_FavoriDetail.xaml
    /// </summary>
    public partial class UC_FavoriDetail : UserControl
    {
        public Manager Manager => (App.Current as App).LeManager;

        public UC_FavoriDetail()
        {
            InitializeComponent();
            DataContext = Manager;
        }

        private void AjouterFavori_Click(object sender, RoutedEventArgs e)
        {
            if (Manager.Favoris.Contains(Manager.ItemSélectionné))
            {
                MessageBox.Show("Attention ! Cet objet est déjà un favori !",
                                "Ajout impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (Manager.ItemSélectionné == null)
            {
                MessageBox.Show("Attention ! Aucun objet n'est sélectionné !",
                                "Ajout impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Manager.AjouteFavori(Manager.ItemSélectionné);
        }

        private void SupprimerFavori_Click(object sender, RoutedEventArgs e)
        {
            if (Manager.Favoris.Contains(Manager.ItemSélectionné) == false)
            {
                MessageBox.Show("Attention ! Cet objet n'est pas un favori !",
                                "Suppression impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Manager.SupprimeFavori(Manager.ItemSélectionné);
        }

        private void AjouterAUneListe_Click(object sender, RoutedEventArgs e)
        {
            if (Manager.ItemSélectionné == null)
            {
                MessageBox.Show("Attention ! Aucun objet n'est sélectionné !",
                                "Ajout impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var AjouterObjetListe = new AjouterObjetListe();
            AjouterObjetListe.ShowDialog();
        }
    }
}
