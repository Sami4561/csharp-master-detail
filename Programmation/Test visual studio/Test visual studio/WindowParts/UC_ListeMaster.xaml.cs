﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bibliothèque_de_classes;
using System.ComponentModel;

namespace Test_visual_studio.WindowParts
{
    /// <summary>
    /// Logique d'interaction pour UC_ListeMaster.xaml
    /// </summary>
    public partial class UC_ListeMaster : UserControl
    {
        public Manager Manager => (App.Current as App).LeManager;

        public UC_ListeMaster()
        {
            InitializeComponent();
            DataContext = Manager;
        }

        private void TriListe_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            string propertyName = "";
            System.ComponentModel.ListSortDirection sortDirection = System.ComponentModel.ListSortDirection.Ascending;

            switch (b.Name)
            {
                case "mButtonTriAlpha":
                    propertyName = "Nom";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
                case "mButtonTriAjout":
                    propertyName = "DateAjout";
                    sortDirection = System.ComponentModel.ListSortDirection.Ascending;
                    break;
            }
            ListBoxListe.Items.SortDescriptions.Clear();
            ListBoxListe.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription(propertyName, sortDirection));
        }
    }
}
