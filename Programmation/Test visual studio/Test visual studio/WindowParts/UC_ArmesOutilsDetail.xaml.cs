﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bibliothèque_de_classes;

namespace Test_visual_studio.WindowParts
{
    /// <summary>
    /// Logique d'interaction pour UC_ArmesOutilsDetail.xaml
    /// </summary>
    public partial class UC_ArmesOutilsDetail : UserControl
    {
        public Manager Manager => (App.Current as App).LeManager;

        public UC_ArmesOutilsDetail()
        {
            InitializeComponent();
            DataContext = Manager;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
