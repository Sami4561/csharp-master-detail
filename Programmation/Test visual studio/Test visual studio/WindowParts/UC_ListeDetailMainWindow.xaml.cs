﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bibliothèque_de_classes;
using Test_visual_studio.Windows;

namespace Test_visual_studio.WindowParts
{
    /// <summary>
    /// Logique d'interaction pour UC_ListeDetailMainWindow.xaml
    /// </summary>
    public partial class UC_ListeDetailMainWindow : UserControl
    {
        public Manager Manager => (App.Current as App).LeManager;

        public UC_ListeDetailMainWindow()
        {
            InitializeComponent();
            DataContext = Manager;
        }

        private void Modifier_Click(object sender, RoutedEventArgs e)
        {
            if (Manager.ListeSélectionnée == null)
            {
                MessageBox.Show("Attention ! Aucune liste n'est sélectionnée !",
                                "Modification impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var Modifier_Liste = new Modifier_Liste();
            Modifier_Liste.ShowDialog();
        }

        private void Supprimer_Click(object sender, RoutedEventArgs e)
        {
            if (Manager.ListeSélectionnée == null)
            {
                MessageBox.Show("Attention ! Aucune liste n'est sélectionnée !",
                                "Suppression impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Manager.SupprimeListeObjets(Manager.ListeSélectionnée);
        }
    }
}
