﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bibliothèque_de_classes;

namespace Test_visual_studio.WindowParts
{
    /// <summary>
    /// Logique d'interaction pour UC_ObjetListeDetail.xaml
    /// </summary>
    public partial class UC_ObjetListeDetail : UserControl
    {
        public Manager Manager => (App.Current as App).LeManager;

        public UC_ObjetListeDetail()
        {
            InitializeComponent();
            DataContext = Manager;
        }

        private void AjouterObjet_Click(object sender, RoutedEventArgs e)
        {
            if (Manager.ListeSélectionnée.ListeItems.Contains(Manager.ItemSélectionné))
            {
                MessageBox.Show("Attention ! Cet objet est déjà dans la liste !",
                                "Ajout impssible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (Manager.ItemSélectionné == null)
            {
                MessageBox.Show("Attention ! Aucun objet n'est sélectionné !",
                                "Ajout impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var support = Manager.ItemSélectionné;
            Manager.AjouteItemListe(support);
        }

        private void SupprimerObjet_Click(object sender, RoutedEventArgs e)
        {
            if (Manager.ListeSélectionnée.ListeItems.Contains(Manager.ItemSélectionné) == false)
            {
                MessageBox.Show("Attention ! Cet objet n'est pas dans la liste !",
                                "Suppression impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (Manager.ItemSélectionné == null)
            {
                MessageBox.Show("Attention ! Aucun objet n'est sélectionné !",
                                "Suppression impossible",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Manager.SupprimeItemListe(Manager.ItemSélectionné);
        }
    }
}
