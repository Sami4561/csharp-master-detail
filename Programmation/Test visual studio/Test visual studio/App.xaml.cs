﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Bibliothèque_de_classes;
using DataContractPersistance;

namespace Test_visual_studio
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public Manager LeManager { get; private set; } = new Manager(new DataContractPers());

        public App()
        {
            LeManager.ChargeDonnées();
            LeManager.Persistance = new DataContractPersistance.DataContractPers();
            LeManager.SauvegardeDonnées();
        }


    }
}
