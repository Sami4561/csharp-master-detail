﻿using Bibliothèque_de_classes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Stub
{
    public class Stub : IPersistanceManager
    {
        public (IEnumerable<Item> favoris, IEnumerable<ListeObjets> listes) ChargeDonnées()
        {
            List<Item> lesFavoris = ChargeFavoris();
            List<ListeObjets> lesListes = ChargeListes();
            return (lesFavoris, lesListes);
        }

        public void SauvegardeDonnées(IEnumerable<Item> favoris, IEnumerable<ListeObjets> listes)
        {
            Debug.WriteLine("Sauvegarde demandée.");
        }

        private List<Item> ChargeFavoris()
        {
            List<Item> favoris = new List<Item>();
            favoris.Add(new Décoration("Algue", "kelp", "1.13", true, "img/Algue.png", "test"));
            favoris.Add(new ArmesOutils("Arbalète", "crossbow", "1.14", false, "img/Arbalète.png", 326, "6-10"));
            favoris.Add(new Matériaux("Algue séchée", "dried_kelp", "1.13", true, "img/Algue_séchée.png", true));
            favoris.Add(new ArmesOutils("Jambières en diamant", "diamond_leggings", "1.0.0", false, "img/jambières_diamant.png", 496));
            return favoris;
        }

        private List<ListeObjets> ChargeListes()
        {
            List<ListeObjets> listes = new List<ListeObjets>();
            listes.Add(new ListeObjets("Test Liste", new ObservableCollection<Item>(), new DateTime(2020, 5, 31, 16, 09, 00)));
            listes.Add(new ListeObjets("aListe", new ObservableCollection<Item>(), new DateTime(2010, 5, 31, 16, 09, 00)));
            return listes;
        }
    }
}
