﻿using Bibliothèque_de_classes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace DataContractPersistance
{
    [DataContract]
    class DataToPersist
    {
        [DataMember]
        public List<Item> Favoris { get; set; } = new List<Item>();

        [DataMember]
        public List<ListeObjets> Listes { get; set; } = new List<ListeObjets>();
    }
}
