﻿using Bibliothèque_de_classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;

namespace DataContractPersistance
{
    public class DataContractPers : IPersistanceManager
    {
        public string FilePath { get; set; } = Path.Combine(Directory.GetCurrentDirectory(), "..//XML");

        public string FileName { get; set; } = "items.xml";

        string PersFile => Path.Combine(FilePath, FileName);


        public (IEnumerable<Item> favoris, IEnumerable<ListeObjets> listes) ChargeDonnées()
        {
            if(!File.Exists(PersFile))
            {
                throw new FileNotFoundException("Fichier de persistance manquant.");
            }
            var serializer = new DataContractSerializer(typeof(DataToPersist));

            DataToPersist data;
            using (Stream s = File.OpenRead(PersFile))
            {
                data = serializer.ReadObject(s) as DataToPersist;
            }
            return (data.Favoris, data.Listes);
        }

        public void SauvegardeDonnées(IEnumerable<Item> favoris, IEnumerable<ListeObjets> listes)
        {
            var serializer = new DataContractSerializer(typeof(DataToPersist));
            if(!Directory.Exists(FilePath)) // on vérifie que le dossier existe bien, on le crée sinon
            {
                Directory.CreateDirectory(FilePath);
            }

            DataToPersist data = new DataToPersist();
            data.Favoris.AddRange(favoris);
            data.Listes.AddRange(listes);

            var settings = new XmlWriterSettings() { Indent = true };
            using (TextWriter tw = File.CreateText(PersFile))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    serializer.WriteObject(writer, data);
                }
            }
        }
    }
}
